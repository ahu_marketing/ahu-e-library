# coding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

spider = Mechanize.new

kantakji_base_url = 'http://kantakji.com'
kantakji_page = spider.get(kantakji_base_url)
kcl = kantakji_page.links_with(:dom_class => "categoryLink")
kcl = kcl[1..kcl.count-1]

kcl.each do |category_link|

  category_link_page = category_link.click
  articles = category_link_page.root.xpath("//li[@class='liArticle']").to_a

  counter = 0
  
  articles.each do |article|

    
    meta_data = {}

    begin
      meta_data[:category] = category_link.text.strip || ""
      meta_data[:author] = article.children[1].children[1].children.text.strip || ""
      meta_data[:file_title] = article.children[3].children.text.strip || ""
      meta_data[:file_url_remote] = kantakji_base_url + article.children[5].children[1].children[1].attributes["href"].value || ""
      meta_data[:file_name_local] = File.basename article.children[5].children[1].children[1].attributes["href"].value.strip
      meta_data[:file_location] = "/files/" + meta_data[:file_name_local] || ""
    rescue
      p 'woops :)'
    end

    counter += 1

    `wget "#{meta_data[:file_url_remote]}" -P "#{Rails.root.to_s}/public/files"`

    puts "files:#{counter}"

    
    Document.create ({file_title: meta_data[:file_title],
                      author: meta_data[:author],
                      categories: [] << meta_data[:category],
                      file_location: meta_data[:file_location],
                      meta_data: meta_data})

    puts "AlhamdulilLah"

  end
end


