json.array!(@documents) do |document|
  json.extract! document, :id, :file_title, :author, :file_url, :category
  json.url document_url(document, format: :json)
end
