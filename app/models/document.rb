class Document

  include Mongoid::Document
  include Elasticsearch::Model

  field :file_title
  field :author
  field :categories, type: Array
  field :file_location
  field :meta_data, type: Hash
  attr_accessible :meta_data, :file_title, :author, :file_location, :category if respond_to? :attr_accessible

  def as_indexed_json(options={})
    as_json(except: [:id, :_id])
  end
  
end







